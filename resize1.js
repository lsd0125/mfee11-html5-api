const Jimp = require('jimp');

let s = 128
Jimp.read(__dirname + '/public/images/draw (1).png').then(image => {
    image
        .resize(s, s, Jimp.RESIZE_BEZIER)
        .write(__dirname + `/public/myApp/images/icons/icon-${s}x${s}.png`);
});